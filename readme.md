#Docnet Programming Challenge

**Name:** Razvan Cristian Marian

**Email:** [razvan.marian@igeek.ro](razvan.marian@igeek.ro)

**Website:** [http://www.ied3vil.com](http://www.ied3vil.com)

##Information

Code Location: `src/`

I have also created a `public` folder and an `app` folder to simulate a real life scenario.

I have also created a `src/ThirdParty` namespace to simulate the libraries used for
sending the consignments. The classes found here simulate real libraries used for different
sending methods. Alternatively i can provide an alternative to the current codebase
where the consignments are grouped and sent in batches.

Running the app currently just dumps the result in `storage/docnet.log` and outputs a "Done" message.
I have included a sample result of a demo in the `docnet.log` file.

For more clarity, i have added a number of readme files to help understand what you are looking at
when browsing the repository.

The vendor folder contains a couple of dependencies i've used to do a working demo. I use
`monolog/monolog` for logging events to files and i have borrowed a random string generation
 function from Laravel's support package, `illuminate/support`. They also brought a few
 dependencies with them.

##To run a demo
Just clone the code, run `composer update` and access `public/index.php` in the browser.
I have included the vendor folder in the repo just so you can skip the composer update part. Also,
make sure the `storage/docnet.log` file is writable by the webserver.

The results can be seen in the log file found here: `storage/docnet.log`.
 The log file is set to clear every time the app runs.
To disable this, just change the init function's parameter from true to false
 in `public/index.php`.