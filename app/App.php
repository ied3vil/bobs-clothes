<?php
namespace BobsClothing;

use Docnet\Batch;
use Docnet\Consignment;
use Docnet\Couriers\RoyalMail;
use Docnet\Couriers\Anc;
use Docnet\Couriers\Hermes;
use Docnet\Cron;
use Docnet\Log;

class App
{
    /**
     * @var Batch
     */
    protected $batch;

    /**
     * @var Cron
     */
    protected $cron;

    /**
     * App initialization function
     * @param bool $clear
     * @return $this
     */
    public function init($clear = false)
    {
        Log::init(LOG_PATH);
        if ($clear) {
            Log::clear();
        }
        $this->cron = new Cron();
        return $this;
    }

    /**
     * App run function
     */
    public function run()
    {
        //simulate cron hitting to start new batch
        $this->batch = $this->cron->createBatch();
        $this->batch->addConsignment(new Consignment(new RoyalMail()));
        $this->batch->addConsignment(new Consignment(new Anc()));
        $this->batch->addConsignment(new Consignment(new Hermes()));
        $this->batch->addConsignment(new Consignment(new RoyalMail()));
        $this->batch->addConsignment(new Consignment(new Hermes()));
        $this->batch->addConsignment(new Consignment(new Hermes()));
        $this->batch->addConsignment(new Consignment(new Anc()));
        $this->batch->addConsignment(new Consignment(new RoyalMail()));
        $this->batch->addConsignment(new Consignment(new RoyalMail()));
        $this->batch->addConsignment(new Consignment(new Anc()));
        $this->batch->addConsignment(new Consignment(new Hermes()));
        $this->cron->setBatch($this->batch)->endBatch();
        echo 'Done!';
    }
}