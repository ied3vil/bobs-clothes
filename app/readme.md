#The app folder

Created to simulate a real-life scenario. Currently the App class only initializes the Log class
and has a run() function that simulates cron jobs hitting to  a batch, adds consignments and
another cron simulation ends the current batch, sending the consignments. The cron implementation is crude,
and it is done just so we can simulate a real life model.