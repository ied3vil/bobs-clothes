#The Source Code

##Batch Logic
`Batch.php` - is the main batch class, where the required logic is located.

##Consignments
`Consignment.php` - the consignment Class.

##Couriers
The `Couriers` folder contains the individual consignments classes that implement the
`CourierInterface` interface and use the `CourierTrait` trait. I have created three
Courier classes for 3 different couriers used: `Anc`, `RoyalMail` and `Hermes`.

##Log class - used for demo
`Log.php` - Class used to do logging, it is just a wrapper for the Monolog class found in the
vendor folder, for simplicity.

##3rd Party libraries simulation
The `ThirdParty` folder contains mock-up classes that simulate a real life scenario for
sending the consignments.

