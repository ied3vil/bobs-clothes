<?php
namespace Docnet\Couriers;

use Docnet\Consignment;
use Docnet\CourierInterface;
use Docnet\CourierTrait;
use Docnet\ThirdParty\AnonymousFTP;

class Anc implements CourierInterface
{
    use CourierTrait;

    /**
     * Stores the courier's name
     */
    const NAME = 'ANC';

    /**
     * @var AnonymousFTP
     */
    protected $anonymousFtp;

    public function __construct()
    {
        $this->anonymousFtp = new AnonymousFTP();
    }

    /**
     * Sends a consignment
     * @param Consignment $consignment
     */
    public function send(Consignment $consignment)
    {
        $this->anonymousFtp->send('Sent ' . $this->getName() . ' consignment [' . $consignment->getKey() . ']');
    }

    /**
     * Generator for the unique consignment identifier
     * @return string
     */
    public function generateKey()
    {
        return $this->getName() . '_' . $this->randomString(15);
    }

    /**
     * Returns the name of the courier
     * @return string
     */
    public function getName()
    {
        return $this::NAME;
    }
}