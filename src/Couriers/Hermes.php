<?php
namespace Docnet\Couriers;

use Docnet\Consignment;
use Docnet\CourierInterface;
use Docnet\CourierTrait;
use Docnet\ThirdParty\PostAPI;

class Hermes implements CourierInterface
{
    use CourierTrait;

    /**
     * Stores the courier's name
     */
    const NAME = 'Hermes';

    /**
     * @var PostAPI
     */
    protected $postAPI;

    public function __construct()
    {
        $this->postAPI = new PostAPI();
    }

    /**
     * Sends a consignment
     * @param Consignment $consignment
     */
    public function send(Consignment $consignment)
    {
        $this->postAPI->post('Sent ' . $this->getName() . ' consignment [' . $consignment->getKey() . ']');
    }

    /**
     * Generator for the unique consignment identifier
     * @return string
     */
    public function generateKey()
    {
        return $this->getName() . '_' . $this->randomString(15);
    }

    /**
     * Returns the name of the courier
     * @return string
     */
    public function getName()
    {
        return $this::NAME;
    }
}