<?php
namespace Docnet\Couriers;

use Docnet\Consignment;
use Docnet\CourierInterface;
use Docnet\CourierTrait;
use Docnet\ThirdParty\Mailer;

class RoyalMail implements CourierInterface
{
    use CourierTrait;

    /**
     * Stores the courier's name
     */
    const NAME = 'RoyalMail';

    /**
     * @var Mailer
     */
    protected $mailer;

    public function __construct()
    {
        $this->name = 'Royal Mail';
        $this->mailer = new Mailer();
    }

    /**
     * Sends a consignment
     * @param Consignment $consignment
     */
    public function send(Consignment $consignment)
    {
        $this->mailer->send('Sent ' . $this->getName() . ' consignment [' . $consignment->getKey() . ']');
    }

    /**
     * Generator for the unique consignment identifier
     * @return string
     */
    public function generateKey()
    {
        return $this->getName() . '_' . $this->randomString(15);
    }

    /**
     * Returns the name of the courier
     * @return string
     */
    public function getName()
    {
        return $this::NAME;
    }
}