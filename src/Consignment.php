<?php


namespace Docnet;


class Consignment
{
    /**
     * Stores the unique number for the consignment
     * @var string
     */
    protected $key;
    
    /**
     * Courier Class
     * @var CourierInterface
     */
    protected $courier;

    public function __construct(CourierInterface $courier)
    {
        $this->courier = $courier;
        $this->setKey($this->courier->generateKey());
    }

    /**
     * Sends the current consignment
     */
    public function send()
    {
        $this->courier->send($this);
    }

    /**
     * Returns the unique number
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Sets the unique number
     * @param $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }
}