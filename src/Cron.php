<?php
namespace Docnet;

/**
 * Class Cron
 * @package Docnet
 */
class Cron
{
    /**
     * @var Batch
     */
    protected $batch;

    /**
     * Creates a new batch and returns it
     * @return Batch
     */
    public function createBatch()
    {
        $this->batch = new Batch();
        $this->batch->start();
        return $this->batch;
    }

    /**
     * Sets the current batch
     * @param Batch $batch
     * @return $this
     */
    public function setBatch(Batch $batch)
    {
        $this->batch = $batch;
        return $this;
    }

    /**
     * Ends the batch
     */
    public function endBatch()
    {
        $this->batch->end();
    }
}