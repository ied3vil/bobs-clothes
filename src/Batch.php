<?php
namespace Docnet;

/**
 * Class Batch
 * @package Docnet
 */
class Batch
{
    /**
     * Will store the consignments as array of objects
     * @var array
     */
    protected $consignments;

    /**
     * Stores batch name
     * @var string
     */
    protected $name;

    /**
     * Starts a new batch
     */
    public function start()
    {
        $this->name = date('Y-m-d');
        $this->consignments = [];
        Log::add('Batch ' . $this->name . ' started.');
    }

    /**
     * Adds a consignment to the current batch
     * @param Consignment $consignment
     */
    public function addConsignment(Consignment $consignment)
    {
        $this->consignments[] = $consignment;
    }

    /**
     * Ends the current batch
     */
    public function end()
    {
        /**
         * @var Consignment $consignment
         */
        foreach ($this->consignments as $consignment) {
            $consignment->send();
        }
        Log::add('Batch ' . $this->name . ' ended.');
    }
}