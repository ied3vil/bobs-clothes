<?php
namespace Docnet;

use Illuminate\Support\Str;

/**
 * Class CourierTrait
 * @package Docnet
 */
trait CourierTrait
{
    /**
     * Simulation of the unique number generator
     * @param $length
     * @return string
     */
    private function randomString($length)
    {
        return Str::random($length);
    }
}