<?php
namespace Docnet\ThirdParty;
use Docnet\Log;

/**
 * Class Mailer
 * Mocks a mailer library
 * @package Docnet\ThirdParty
 */
class Mailer
{
    /**
     * Sends the data using email
     * @param $data
     */
    public function send($data)
    {
        Log::add($data);
    }
}