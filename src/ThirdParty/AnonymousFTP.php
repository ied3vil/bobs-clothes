<?php
namespace Docnet\ThirdParty;
use Docnet\Log;

/**
 * Class AnonymousFTP
 * Mocks an Anonymous FTP Library
 * @package Docnet\ThirdParty
 */
class AnonymousFTP
{
    /**
     * Sends the data using Anonymous FTP
     * @param $data
     */
    public function send($data)
    {
        Log::add($data);
    }
}