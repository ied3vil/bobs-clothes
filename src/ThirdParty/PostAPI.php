<?php
namespace Docnet\ThirdParty;

use Docnet\Log;

/**
 * Class PostAPI
 * Mocks a Post API Library
 * @package Docnet\ThirdParty
 */
class PostAPI
{
    /**
     * Sends the data through an API
     * @param $data
     */
    public function post($data)
    {
        Log::add($data);
    }
}