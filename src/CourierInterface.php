<?php
namespace Docnet;

/**
 * Interface CourierInterface
 * @package Docnet
 */
interface CourierInterface
{
    /**
     * Sends a consignment
     * @param Consignment $consignment
     * @return mixed
     */
    public function send(Consignment $consignment);

    /**
     * Gets the courier's name
     * @return string
     */
    public function getName();

    /**
     * Generates new unique identifier for consignments
     * @return string
     */
    public function generateKey();
}