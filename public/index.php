<?php
require '../vendor/autoload.php';
define('LOG_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR . 'docnet.log'));

use BobsClothing\App;

$app = new App;
$app->init(true)->run();